package com.sam.java.design;

/**
 * 饿汉 变种
 * 这种方式基于classloder机制避免了多线程的同步问题，不过，instance在类装载时就实例化，这时候初始化instance显然没有达到lazy loading的效果。
 */
public class Singleton4 {
    private static Singleton4 instance = null;

    static {
        instance = new Singleton4();
    }
    private Singleton4() {
    }

    private static Singleton4 getInstance() {
        return instance;
    }
}
