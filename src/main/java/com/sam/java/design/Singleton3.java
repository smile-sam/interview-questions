package com.sam.java.design;

/**
 * 饿汉
 * 表面上看起来差别挺大，其实更第三种方式差不多，都是在类初始化即实例化instance
 */
public class Singleton3 {
    private static Singleton3 instance = new Singleton3();

    private Singleton3() {
    }

    private static Singleton3 getInstance() {
        return instance;
    }
}
