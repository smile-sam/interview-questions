package com.sam.java.design;

/**
 * 懒汉 线程安全
 * 这种写法在getInstance()方法中加入了synchronized锁。能够在多线程中很好的工作，而且看起来它也具备很好的lazy loading，但是效率很低（因为锁），并且大多数情况下不需要同步。
 */
public class Singleton2 {
    private static Singleton2 instance;

    private Singleton2() {
    }

    private static synchronized Singleton2 getInstance() {
        if (instance == null) {
            instance = new Singleton2();
        }
        return instance;
    }
}
