package com.sam.java.basic;

/**
 * Created by sam on 2018/6/28.
 */
public class Ex {
    public static void main(String[] args) {
        int a = 13;
        a = a / 5;

        /**
         * 啥也不会a
         a是int类型,13/5得到一个浮点类型,浮点类型转成整数类型的原则是,不四舍五入,直接截断小数点后的部分
         */
        System.out.println(a); // 2
    }
}