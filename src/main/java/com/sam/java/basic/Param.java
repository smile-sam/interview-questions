package com.sam.java.basic;

/**
 * Created by sam on 2018/6/28.
 */
public class Param {

    /**
     * 链接：https://www.nowcoder.com/questionTerminal/a52f844a5f364be58fcb0aa7ab103f68
     来源：牛客网

     1.形参：用来接收调用该方法时传递的参数。只有在被调用的时候才分配内存空间，一旦调用结束，就释放内存空间。因此仅仅在方法内有效。

     2.实参：传递给被调用方法的值，预先创建并赋予确定值。

     3.java的基本数据类型是传值调用，对象引用类型是传引用。
     4.当传值调用时，改变的是形参的值，并没有改变实参的值，实参的值可以传递给形参，但是，这个传递是单向的，形参不能传递回实参。

     5.当引用调用时，如果参数是对象，无论对对象做了何种操作，都不会改变实参对象的引用，但是如果改变了对象的内容，就会改变实参对象的内容。
     */


    public static void main(String[] args) throws Exception{
        String s = "hello world";
        changeStr(s);
        System.out.println(s); // hello world


        s = changeStr1(s);
        System.out.println(s); // welcome


        StringBuffer ss = new StringBuffer("hello");
        changeStringBuffer(ss);
        System.out.println(ss); //
    }


    public static void changeStr(String s) {
        s = "welcome";
    }


    public static String changeStr1(String s) {
        s = "welcome";
        return s;
    }


    private static void changeStringBuffer(StringBuffer s) {
        s.append(" sam ");
    }
}
