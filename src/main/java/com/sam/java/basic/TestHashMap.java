package com.sam.java.basic;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sam on 2018/7/4.
 */
public class TestHashMap {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();

        String value1 = map.put(12, "bbb");
        System.out.println("value1:\t" + map.get(12));
        String value2 = map.put(12, "ddd");
        System.out.println("value2:\t" + value2);
        String value3 =  map.put(108, "eee");
        System.out.println("value3:\t" + value3);
       System.out.println( map.get(12));

    }
}
