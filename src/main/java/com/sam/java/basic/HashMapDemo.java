package com.sam.java.basic;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by sam on 2018/7/4.
 */
public class HashMapDemo {


    public static void main(String[] args) {


        final HashMap<String, String> map = new HashMap<String, String>(2);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            map.put(UUID.randomUUID().toString(), "");
                        }
                    }, "ftf" + i).start();
                }
            }
        }, "ftf");
        t.start();

    }
}
