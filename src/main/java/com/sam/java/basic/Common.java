package com.sam.java.basic;

/**
 * Created by sam on 2018/6/27.
 */
public class Common {

    /**
     * 常见字符的ASCII码值如下：空格的ASCII码值为32；
     * 数字0到9的ASCII码值分别为48到57；
     * 大写字母“A”到“Z”的ASCII码值分别为65到90；
     * 小写字母“a”到“z”的ASCII码值分别为97到到122。
     */

    // 执行语句“int a= ’ 2 ’ ”后，a的值是（ ） 答案  50


    /**
     * JAVA关键字：abstract，boolean,break,back,continue,case,catch,char ,class,do,double,default,else,extends,final,finally,float,for,long,if,implements,import,new,instanceof,int,interface,package,private,public,protected,return,short,static,super,switch,sychronized,this,while,void,throw,throws,try,volatile 保留字：const,go
     */



    //=========================

    /**
     * 大多数 JVM 将内存区域划分为 Method Area（Non-Heap）（方法区） ,Heap（堆） , Program Counter Register（程序计数器） ,
     * VM Stack（虚拟机栈，也有翻译成JAVA 方法栈的）,
     * Native Method Stack  （ 本地方法栈 ），其中Method Area 和  Heap 是线程共享的 ,
     * VM Stack，Native Method Stack  和Program Counter Register
     * 是非线程共享的。为什么分为 线程共享和非线程共享的呢?请继续往下看。

     首先我们熟悉一下一个一般性的 Java 程序的工作过程。一个 Java 源程序文件，
     会被编译为字节码文件（以 class 为扩展名），每个java程序都需要运行在自己的JVM上，
     然后告知 JVM 程序的运行入口，再被 JVM 通过字节码解释器加载运行。那么程序开始运行后，
     都是如何涉及到各内存区域的呢？

     概括地说来，JVM初始运行的时候都会分配好 Method Area（方法区） 和Heap（堆） ，
     而JVM 每遇到一个线程，就为其分配一个 Program Counter Register（程序计数器） ,
     VM Stack（虚拟机栈）和Native Method Stack
     （本地方法栈）， 当线程终止时，三者（虚拟机栈，本地方法栈和程序计数器）所占用的内存空间也会被释放掉。
     这也是为什么我把内存区域分为线程共享和非线程共享的原因，非线程共享的那三个区域的生命周期与所属线程相同，
     而线程共享的区域与JAVA程序运行的生命周期相同，
     所以这也是系统垃圾回收的场所只发生在线程共享的区域（实际上对大部分虚拟机来说知发生在Heap上）的原因。

     */
    /**
     * 匿名内部类的创建格式为： new 父类构造器（参数列表）|实现接口（）{
     //匿名内部类的类体实现
     }
     使用匿名内部类时，必须继承一个类或实现一个接口
     匿名内部类由于没有名字，因此不能定义构造函数
     匿名内部类中不能含有静态成员变量和静态方法
     * @param userId
     * @return
     */

    public static boolean isAdmin(String userId) {
        String v = userId.toLowerCase();
        return  v == "admin";
    }

    public static void main(String[] args) {
        System.out.println(isAdmin("Admin"));
    }
}
