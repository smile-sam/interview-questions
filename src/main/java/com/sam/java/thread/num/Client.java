package com.sam.java.thread.num;

public class Client {

    public static void main(String[] args) {
        Num num = new Num();

        PrintOdd printOdd = new PrintOdd(num);
        PrintEven printEven = new PrintEven(num);

        Thread t1 = new Thread(printOdd);
        Thread t2 = new Thread(printEven);

        t1.start();
        t2.start();
    }
}
