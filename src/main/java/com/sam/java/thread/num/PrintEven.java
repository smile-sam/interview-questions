package com.sam.java.thread.num;

public class PrintEven implements Runnable{

    Num num;

    public PrintEven(Num num) {
        this.num = num;
    }

    @Override
    public void run() {
        while (num.i < 100) {
            synchronized (num) { // 必须使用同一锁对象 num
                if(!num.flag) {
                    try{
                        num.wait(); // wait() 函数必须和锁的对象是同一个
                    } catch (Exception e) {

                    }
                } else {
                    System.out.println("偶数-------" +num.i);
                    num.i++;
                    num.flag = false;
                    num.notify();
                }
            }
        }
    }
}
