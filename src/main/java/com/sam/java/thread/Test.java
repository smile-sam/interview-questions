package com.sam.java.thread;

import java.util.Scanner;

/*线程的冲突
    在多个线程访问同一个对象的同一个属性时，才会出现线程冲突，线程冲突会造成数据丢失、重复等严重问题。
   为了避免这个问题可以synchronized关键字让线程同步。


   */
public class Test {
    String name;
    public static void main(String[] args) {
        final Test obj=new Test();
        final Test obj2=new Test();
        final Thread t=new Thread(){
            @Override
            public  void run(){
                while (true){
                    try {
                        obj.setName("张三");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
        final Thread t2=new Thread(){
            @Override
            public  void run(){
                while (true){
                    try {
                        obj2.setName("李四");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t2.start();
    }
    /*
    1，放在函数中锁住整个函数，只有前一个线程执行完下一个函数才能访问
    2，放在代码块中，锁住需要共享的资源，推荐使用
    */
    public /*synchronized*/  void setName(String name){

        synchronized(this) {
        this.name = name;
        equalsName(name);
        }
    }
    public void equalsName(String name){
        if(name!=this.name)
            System.out.println(name+" "+this.name);
    }
}