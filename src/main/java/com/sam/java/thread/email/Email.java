package com.sam.java.thread.email;

import java.util.List;
import java.util.Map;

public class Email {
    List<String> email;
    Map<String, Boolean> map;


    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public Map<String, Boolean> getMap() {
        return map;
    }

    public void setMap(Map<String, Boolean> map) {
        this.map = map;
    }
}
