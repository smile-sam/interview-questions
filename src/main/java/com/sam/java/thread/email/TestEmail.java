package com.sam.java.thread.email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestEmail {

    public static void main(String[] args) {
        List<String> emails = getEmailList();
        List<String> emails1 = emails.subList(0, 300000);

        List<String> emails2 = emails.subList(300001, 600000);

        List<String> emails3 = emails.subList(600001, emails.size());

        long startTime = System.currentTimeMillis();
        System.out.println("start ---- " + startTime);
//        for(String email : emails) {
//            isEmail(email);
//        }

        Map<String, Boolean> map = new HashMap<>();
        EmailThread emailThread1 = new EmailThread(emails1);
        EmailThread emailThread2 = new EmailThread(emails2);
        EmailThread emailThread3 = new EmailThread(emails3);
        Thread t1 = new Thread(emailThread1);


        Thread t2 = new Thread(emailThread2);


        Thread t3 = new Thread(emailThread3);

        t1.start();
        t2.start();
        t3.start();
        System.out.println(emailThread1.getMap().size());
        System.out.println(emailThread2.getMap().size());
        System.out.println(emailThread3.getMap().size());
        long endTime = System.currentTimeMillis();
        System.out.println("endTime ---- " + endTime);

        System.out.println("耗时：" + (endTime - startTime) / 1000);
    }

    public static List<String> getEmailList() {
        List<String> emails = new ArrayList<>(10000);
        for (int i = 0; i < 1000000; i++) {
            emails.add("a" + i + "qq.com");
            if (i % 16 == 0) {
                emails.add("b---" + i + ".com");
            }
        }
        return emails;
    }


    /**
     * 检测邮箱地址是否合法
     *
     * @param email
     * @return true合法 false不合法
     */
    public static boolean isEmail(String email) {
        if (null == email || "".equals(email)) return false;
//        Pattern p = Pattern.compile("\\w+@(\\w+.)+[a-z]{2,3}"); //简单匹配
        Pattern p = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");//复杂匹配
        Matcher m = p.matcher(email);
        return m.matches();
    }


}
