package com.sam.java.redis;

public class LockRes {
    // 是否拿到锁，false：没拿到，true：拿到
    private boolean flag;
    // 缓存的键
    private String key;
    // 缓存的值
    private String value;

    public LockRes(boolean flag, String key, String value) {
        super();
        this.flag = flag;
        this.key = key;
        this.value = value;
    }
    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
