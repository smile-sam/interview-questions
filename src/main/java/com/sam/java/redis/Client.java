package com.sam.java.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class Client {



    @Autowired
    public StringRedisTemplate stringRedisTemplate;

    @RequestMapping("/test1")
    public void test1() {
        LockRes lockRes = RedisLock.tryLock("abc",stringRedisTemplate,30);
        try {
            boolean flag = lockRes.isFlag();
            String key = lockRes.getKey();
            String value = lockRes.getValue();

        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            RedisLock.unlock(lockRes,stringRedisTemplate);
        }

    }

    @RequestMapping("/test2")
    public void test2() {
        RedisLock.tryLock("abc",stringRedisTemplate,30);
        LockRes lockRes = RedisLock.tryLock("abc",stringRedisTemplate,30);
        try {
            boolean flag = lockRes.isFlag();
            String key = lockRes.getKey();
            String value = lockRes.getValue();

        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            RedisLock.unlock(lockRes,stringRedisTemplate);
        }
    }
    public static void main(String[] args) {

    }
}
